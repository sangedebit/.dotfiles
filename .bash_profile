#############################
#
#    aliases
#
#############################
alias cd..='cd ..'
alias vim='mvim -v'





#############################
#
#    colors
#
#############################
COLBROWN="\[\033[1;33m\]"
COLRED="\[\033[1;31m\]"
COLGREEN="\[\033[1;32m\]"
COLCLEAR="\[\033[0m\]"
COLBLUE="\e[1;34m"





#############################
#
#    command history
#
#############################

# Avoid duplicates
export HISTCONTROL=ignoredups:erasedups
# When the shell exits, append to the history file instead of overwriting it
shopt -s histappend

# After each command, append to the history file and reread it
export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"






#############################
#
#    git and folder 2 deep
#
#############################

function we_are_in_git_work_tree {
    git rev-parse --is-inside-work-tree &> /dev/null
}

function parse_git_branch {
    if we_are_in_git_work_tree
    then
    local BR=$(git rev-parse --symbolic-full-name --abbrev-ref HEAD 2> /dev/null)
    if [ "$BR" == HEAD ]
    then
        local NM=$(git name-rev --name-only HEAD 2> /dev/null)
        if [ "$NM" != undefined ]
        then echo -n " @$NM "
        else git rev-parse --short HEAD 2> /dev/null
        fi
    else
        echo -n " ⎇  $BR "
    fi
    fi
}

function parse_git_status {
    if we_are_in_git_work_tree
    then
    local ST=$(git status --short 2> /dev/null)
    local COUNT=$(git status | grep 'modified:' | wc -l 2> /dev/null)
    local COUNT_NO_WHITESPACE="$(echo -e "${COUNT}" | sed -e 's/^[[:space:]]*//')"
    if [ -n "$ST" ]
    then echo -n "Δ$COUNT_NO_WHITESPACE" #symbol for having changes
  else echo -n "✓" #symbol for no changes
    fi
    fi
}

function pwd_depth_limit_2 {
    if [ "$PWD" = "$HOME" ]
    then echo -n "~"
  else pwd | sed -e "s|.*/\(.*/.*\)|\1|"
    fi
}







#############################
#
#    prompt
#
#############################
export -f parse_git_branch parse_git_status we_are_in_git_work_tree pwd_depth_limit_2
export PS1="\n\n\n\n$COLRED<$COLBROWN \$(pwd_depth_limit_2)$COLBLUE\$(parse_git_branch)$COLRED\$(parse_git_status)$COLRED >$COLCLEAR \n$ "
export TERM="xterm-color"
