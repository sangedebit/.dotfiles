syntax on

set backspace=indent,eol,start " backspace over everything in insert mode
set number "show gutter numbers
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE " change gutter color
set tabstop=4
set laststatus=2 " always show a line
set statusline="%f%m%r%h%w [%Y] [0x%02.2B]%< %F%=%4v,%4l %3p%% of %L" " show current info with ^g shortcut
set wildignore+=*/tmp/*,*/bower_components/*,*/node_modules/* " vims wildignore dont want to search temp files
set clipboard=unnamed " vim y yank copies to os clipboard


:map :search :Ag
:imap jj <Esc>


" CtrlP 
let g:ctrlp_map = '<Leader>t'
let g:ctrlp_match_window_bottom = 0
let g:ctrlp_match_window_reversed = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_dotfiles = 0
let g:ctrlp_switch_buffer = 0



" indent colors settings
"let g:indent_guides_enable_on_vim_startup = 1
"let g:indent_guides_guide_size = 1
"let g:indent_guides_auto_colors = 0
"autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=3
"autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=4


" automatic installation
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall | source $MYVIMRC
endif


call plug#begin('~/.vim/plugged')
" plugins go here
Plug 'https://github.com/ctrlpvim/ctrlp.vim' " :ctrlP fuzzy autocomplete for loading files
Plug 'https://github.com/easymotion/vim-easymotion' " easy motion - quickly open files
Plug 'https://github.com/Raimondi/delimitMate' " automatic closing of quotes, parenthesis, brackets, etc.
Plug 'https://github.com/rking/ag.vim' " search for a lot of stuff across a lot of files
Plug 'https://github.com/Valloric/YouCompleteMe', { 'do': './install.py' } " fuzzy-search code completion
Plug 'https://github.com/mhinz/vim-signify' " show git changes in gutter
Plug 'https://github.com/mustache/vim-mustache-handlebars' " working with mustache and handlebars templates
Plug 'https://github.com/sukima/xmledit/' " close html tags with <tag>> creates <tag></tag> - had to copy ftplugin/xml.vim to html.vim and hbs.vim
Plug 'https://github.com/terryma/vim-multiple-cursors' " multi line/instance edit
Plug 'https://github.com/tpope/vim-sleuth' " automatically adjust indentaion - shift width and expandtab for current file
" Plug 'https://github.com/nathanaelkane/vim-indent-guides' " display indentation type
call plug#end()
